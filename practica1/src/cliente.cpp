#include "glut.h"
#include "MundoCliente.h"


CMundo mundo;

//GLUT llama automaticamente a las funciones callback cuando ocurre un evento

void OnDraw(void); 
void OnTimer(int value);
void OnKeyboardDown(unsigned char key, int x, int y);	



int main(int argc,char* argv[])
{

	//Inicializar el gestor de ventanas GLUT
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Cliente");


	//Registrar los callbacks

	glutDisplayFunc(OnDraw);
	glutTimerFunc(25,OnTimer,0);//cada 25ms llama a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);	

	mundo.InitGL();

	//pasarle el control a GLUT,que llamara a los callbacks

	glutMainLoop();

	return 0;   

}



void OnDraw(void)
{
	mundo.OnDraw();
}

void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}

void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();

}
